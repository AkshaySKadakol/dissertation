package com.dissertation.bits.client.controller;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dissertation.bits.client.model.ServiceInfo;
import com.dissertation.bits.client.model.Task;
import com.dissertation.bits.client.service.ServiceManager;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.StringConverter;

@Component
public class StatusTabController {

  private static final Logger logger =
      LoggerFactory.getLogger(StatusTabController.class);

  private final ServiceManager serviceManager;

  @FXML
  private ListView<ServiceInfo> serviceList;

  @FXML
  private ListView<Task> taskList;

  @FXML
  private GridPane editServiceGrid;

  @FXML
  private TextField allocatedMemory, allocatedCpu, scale;

  @FXML
  private Text statusText;

  @FXML
  private Button refreshButton, updateButton, deleteButton;

  private ScheduledExecutorService executor =
      Executors.newSingleThreadScheduledExecutor();

  private TabPaneManager tabPaneManager;

  @Autowired
  public StatusTabController(ServiceManager serviceManager) {
    this.serviceManager = serviceManager;
  }

  public void setTabPaneManager(TabPaneManager tabPaneManager) {
    this.tabPaneManager = tabPaneManager;
  }

  public void initialize() {
    serviceList.getSelectionModel().selectedItemProperty()
        .addListener(new ChangeListener<ServiceInfo>() {

          @Override
          public void changed(ObservableValue<? extends ServiceInfo> observable,
              ServiceInfo oldValue, ServiceInfo newValue) {
            logger.info("Selected item {}", newValue);
            if (newValue != null) {
              updateTasksList(newValue);
              deleteButton.setDisable(true);
            }
          }
        });
    serviceList.setCellFactory(lv -> {
      TextFieldListCell<ServiceInfo> cell = new TextFieldListCell<>();
      cell.setConverter(new StringConverter<ServiceInfo>() {

        @Override
        public String toString(ServiceInfo object) {
          return object.getServiceName();
        }

        @Override
        public ServiceInfo fromString(String string) {
          ServiceInfo serviceInfo = cell.getItem();
          serviceInfo.setServiceName(string);
          return serviceInfo;
        }
      });
      return cell;
    });
  }

  public void setup() {
    serviceList.getItems().clear();
    refreshButton.setDisable(true);
    taskList.getItems().clear();
    editServiceGrid.setVisible(false);
    deleteButton.setDisable(true);
    tabPaneManager.disableServiceTab(true);
    tabPaneManager.disableSwarmTab(true);
    javafx.concurrent.Task<List<ServiceInfo>> task =
        new javafx.concurrent.Task<List<ServiceInfo>>() {
          @Override
          protected List<ServiceInfo> call() throws Exception {
            ServiceInfo[] list = serviceManager.getServiceList();
            logger.info("Found services =  {}", Arrays.asList(list));
            return Arrays.asList(list);
          }
        };

    task.setOnSucceeded((event) -> {
      serviceList.setItems(
          FXCollections.observableList(new LinkedList<>(task.getValue())));
      refreshButton.setDisable(false);
      tabPaneManager.disableServiceTab(false);
      tabPaneManager.disableSwarmTab(false);
    });

    task.setOnFailed((event) -> {
      refreshButton.setDisable(false);
      tabPaneManager.disableServiceTab(false);
      tabPaneManager.disableSwarmTab(false);
    });

    Thread thread = new Thread(task);
    thread.setDaemon(true);
    thread.start();
  }

  @FXML
  public void onRefreshButtonClicked(MouseEvent event) {
    setup();
  }

  private void updateTasksList(ServiceInfo serviceInfo) {
    editServiceGrid.setVisible(false);
    taskList.getItems().clear();
    refreshButton.setDisable(true);
    deleteButton.setDisable(true);
    tabPaneManager.disableServiceTab(true);
    tabPaneManager.disableSwarmTab(true);
    javafx.concurrent.Task<List<Task>> task =
        new javafx.concurrent.Task<List<Task>>() {
          @Override
          protected List<Task> call() throws Exception {
            Task[] list = serviceManager
                .getTasksForServiceName(serviceInfo.getServiceName());
            logger.info("Found tasks =  {}", Arrays.asList(list));
            return Arrays.asList(list);
          }
        };

    task.setOnSucceeded((event) -> {
      logger.info("Successfully updated tasks list");
      taskList.setItems(
          FXCollections.observableList(new LinkedList<>(task.getValue())
              .stream().filter(t -> t.getDesiredState().equals("running"))
              .collect(Collectors.toList())));
      refreshButton.setDisable(false);
      deleteButton.setDisable(false);
      allocatedMemory.setText(String.valueOf(serviceInfo.getAllocatedMemory()));
      allocatedCpu.setText(String.valueOf(serviceInfo.getAllocatedCpu()));
      scale.setText(String.valueOf(serviceInfo.getScale()));
      editServiceGrid.setVisible(true);
      tabPaneManager.disableServiceTab(false);
      tabPaneManager.disableSwarmTab(false);
    });

    task.setOnFailed((event) -> {
      logger.error("Failed to update tasks list", task.getException());
      refreshButton.setDisable(false);
      deleteButton.setDisable(false);
      tabPaneManager.disableServiceTab(false);
      tabPaneManager.disableSwarmTab(false);
    });

    Thread thread = new Thread(task);
    thread.setDaemon(true);
    thread.start();
  }

  @FXML
  public void onUpdateButtonClicked(MouseEvent event) {
    ServiceInfo selectedItem =
        serviceList.getSelectionModel().getSelectedItem();
    logger.info(
        "Updating service {} with parameters allocatedMemory = {}, allocatedCpu = {}, scale = {}",
        selectedItem, allocatedMemory.getText(), allocatedCpu.getText(),
        scale.getText());
    refreshButton.setDisable(true);
    updateButton.setDisable(true);
    deleteButton.setDisable(true);
    tabPaneManager.disableServiceTab(true);
    tabPaneManager.disableSwarmTab(true);
    ServiceInfo serviceInfo = new ServiceInfo();
    serviceInfo.setServiceName(selectedItem.getServiceName());
    serviceInfo.setAllocatedCpu(Float.parseFloat(allocatedCpu.getText()));
    serviceInfo.setAllocatedMemory(Integer.parseInt(allocatedMemory.getText()));
    serviceInfo.setScale(Integer.parseInt(scale.getText()));
    javafx.concurrent.Task<ServiceInfo> task =
        new javafx.concurrent.Task<ServiceInfo>() {
          @Override
          protected ServiceInfo call() throws Exception {
            ServiceInfo info = serviceManager.updateService(serviceInfo);
            logger.info("Updated ServiceInfo =  {}", info);
            return info;
          }
        };

    task.setOnSucceeded((e) -> {
      ServiceInfo info = task.getValue();
      logger.info("Successfully updated Service {}", info);
      statusText.setText("Successfully updated Service. Please wait...");

      Runnable runnable = new Runnable() {

        @Override
        public void run() {
          setup();
          updateButton.setDisable(false);
          statusText.setText("");
          tabPaneManager.disableServiceTab(false);
          tabPaneManager.disableSwarmTab(false);
        }
      };

      executor.schedule(() -> Platform.runLater(runnable), 5, TimeUnit.SECONDS);
    });

    task.setOnFailed((e) -> {
      logger.error("Failed to update Service", task.getException());
      statusText.setText("Failed to update Service");
      updateButton.setDisable(false);
      refreshButton.setDisable(false);
      deleteButton.setDisable(false);
      tabPaneManager.disableServiceTab(false);
      tabPaneManager.disableSwarmTab(false);
    });

    Thread thread = new Thread(task);
    thread.setDaemon(true);
    thread.start();
  }

  @FXML
  public void onDeleteButtonClicked(MouseEvent event) {
    ServiceInfo selectedItem =
        serviceList.getSelectionModel().getSelectedItem();
    logger.info("Deleting service = {}", selectedItem);
    refreshButton.setDisable(true);
    updateButton.setDisable(true);
    deleteButton.setDisable(true);
    tabPaneManager.disableServiceTab(true);
    tabPaneManager.disableSwarmTab(true);
    ServiceInfo serviceInfo = new ServiceInfo();
    serviceInfo.setServiceName(selectedItem.getServiceName());
    javafx.concurrent.Task<Boolean> task =
        new javafx.concurrent.Task<Boolean>() {
          @Override
          protected Boolean call() throws Exception {
            return serviceManager.deleteService(serviceInfo);
          }
        };

    task.setOnSucceeded((e) -> {
      logger.info("Successfully deleted Service {}", task.getValue());
      statusText.setText("Successfully deleted Service. Please wait...");
      Runnable runnable = new Runnable() {

        @Override
        public void run() {
          setup();
          updateButton.setDisable(false);
          statusText.setText("");
          tabPaneManager.disableServiceTab(false);
          tabPaneManager.disableSwarmTab(false);
        }
      };

      executor.schedule(() -> Platform.runLater(runnable), 5, TimeUnit.SECONDS);
    });

    task.setOnFailed((e) -> {
      logger.error("Failed to delete Service", task.getException());
      statusText.setText("Failed to delete Service");
      updateButton.setDisable(false);
      refreshButton.setDisable(false);
      tabPaneManager.disableServiceTab(false);
      tabPaneManager.disableSwarmTab(false);
    });

    Thread thread = new Thread(task);
    thread.setDaemon(true);
    thread.start();
  }

  @PreDestroy
  private void tearDown() {
    executor.shutdown();
  }
}
