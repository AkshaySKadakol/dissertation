package com.dissertation.bits.client.model;

public class ClientInfo {

  private long id;

  private String nodeId;

  private int allocateMemoryPercentage;

  private int allocateCpuPercentage;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNodeId() {
    return nodeId;
  }

  public void setNodeId(String nodeId) {
    this.nodeId = nodeId;
  }

  public long getAllocateMemoryPercentage() {
    return allocateMemoryPercentage;
  }

  public void setAllocateMemoryPercentage(int allocateMemoryPercentage) {
    this.allocateMemoryPercentage = allocateMemoryPercentage;
  }

  public long getAllocateCpuPercentage() {
    return allocateCpuPercentage;
  }

  public void setAllocateCpuPercentage(int allocateCpuPercentage) {
    this.allocateCpuPercentage = allocateCpuPercentage;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("class ClientInfo {\n");
    sb.append("  id: ").append(id).append('\n');
    sb.append("  nodeId: ").append(nodeId).append('\n');
    sb.append("  allocateMemoryPercentage: ").append(allocateMemoryPercentage)
        .append('\n');
    sb.append("  allocateCpuPercentage: ").append(allocateCpuPercentage)
        .append('\n');
    sb.append("}\n");
    return sb.toString();
  }

}
