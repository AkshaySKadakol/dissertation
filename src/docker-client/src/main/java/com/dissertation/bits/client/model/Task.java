package com.dissertation.bits.client.model;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Task implements Serializable {

  private static final long serialVersionUID = 5746911828663268492L;

  @JsonProperty("ID")
  private String id;

  private long index;

  @JsonProperty("ServiceID")
  private String serviceId;

  @JsonProperty("Slot")
  private int slot;

  @JsonProperty("NodeID")
  private String nodeId;

  private String state;

  private String message;

  @JsonProperty("DesiredState")
  private String desiredState;

  public String getId() {
    return id;
  }

  public long getIndex() {
    return index;
  }

  @JsonProperty("Version")
  public void setIndex(Map<String, String> version) {
    this.index = Long.parseLong(version.get("Index"));
  }

  public String getServiceId() {
    return serviceId;
  }

  public int getSlot() {
    return slot;
  }

  public String getNodeId() {
    return nodeId;
  }

  @JsonProperty("Status")
  public void setStatus(Map<String, Object> status) {
    this.state = status.get("State").toString();
    this.message = status.get("Message").toString();
  }

  public String getState() {
    return state;
  }

  public String getMessage() {
    return message;
  }

  public String getDesiredState() {
    return desiredState;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ID : ").append(id).append("\n");
    builder.append("Index : ").append(index).append("\n");
    builder.append("ServiceID : ").append(serviceId).append("\n");
    builder.append("Slot : ").append(slot).append("\n");
    builder.append("NodeID : ").append(nodeId).append("\n");
    builder.append("State : ").append(state).append("\n");
    builder.append("Message : ").append(message).append("\n");
    builder.append("DesiredState : ").append(desiredState).append("\n");
    return builder.toString();
  }

  public class Spec {


    public class Resources {

      public class Limits {

      }
    }
  }
}
