package com.dissertation.bits.client.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dissertation.bits.client.model.ClientInfo;
import com.dissertation.bits.client.service.SwarmManager;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.swarm.SwarmInfo;

import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

@Component
public class SwarmTabController {

  private static final Logger logger =
      LoggerFactory.getLogger(SwarmTabController.class);

  @FXML
  private TextField allocatedMemoryPercentage, allocatedCpuPercentage;

  @FXML
  private Button joinButton, updateButton, leaveButton;

  @FXML
  private ProgressIndicator progressIndicator;

  @FXML
  private Text nodeId, statusText;

  private TabPaneManager tabPaneManager;

  @Autowired
  private SwarmManager swarmManager;

  public void setTabPaneManager(TabPaneManager tabPaneManager) {
    this.tabPaneManager = tabPaneManager;
  }

  public void initialize() {
    Task<SwarmInfo> task = new Task<SwarmInfo>() {

      @Override
      protected SwarmInfo call() throws Exception {
        return swarmManager.getSwarmInfo();
      }
    };

    task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

      @Override
      public void handle(WorkerStateEvent event) {
        handleSwarmTaskSuccess(task);
      }
    });

    task.setOnFailed(event -> {
      logger.error("Something went wrong while checking the swarm",
          task.getException());
      statusText.setText("Unknown error.");
    });

    Thread thread = new Thread(task);
    thread.setDaemon(true);
    thread.start();
  }

  private void handleSwarmTaskSuccess(Task<SwarmInfo> swarmInfoTask) {
    SwarmInfo swarmInfo = swarmInfoTask.getValue();
    if (swarmInfo.localNodeState().equals("inactive")) {
      logger.info("Swarm is inactive");
      joinButton.setDisable(false);
      allocatedCpuPercentage.setDisable(false);
      allocatedMemoryPercentage.setDisable(false);
      progressIndicator.setVisible(false);
      allocatedCpuPercentage.setText("50");
      allocatedMemoryPercentage.setText("50");
    } else if (swarmInfo.localNodeState().equals("active")) {
      logger.info("Swarm is active");
      Task<ClientInfo> task = new Task<ClientInfo>() {

        @Override
        protected ClientInfo call() throws Exception {
          return swarmManager.getClientInfo();
        }
      };
      task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

        @Override
        public void handle(WorkerStateEvent event) {
          handleClientInfoTask(task);
        }
      });

      task.setOnFailed(event -> {
        Throwable exception = task.getException();
        logger.error("Something went wrong while getting the Client Info",
            exception);
        statusText.setText(String.format("%s, %s, %s",
            "Something went wrong while connecting to the server",
            "This node may be part of a different swarm - ", exception));
        progressIndicator.setVisible(false);
      });
      Thread thread = new Thread(task);
      thread.setDaemon(true);
      thread.start();
    } else {
      logger.error(
          "Something went wrong during application initialization. Could not get Swarm information.");
      statusText.setText(
          "Something went wrong during application initialization. Could not get Swarm information.");
      progressIndicator.setVisible(false);
    }
  }

  private void handleClientInfoTask(Task<ClientInfo> task) {
    ClientInfo clientInfo = task.getValue();
    logger.info(
        "Successfully retrieved ClientInfo from the server. ClientInfo = {}",
        clientInfo);
    allocatedCpuPercentage
        .setText(String.valueOf(clientInfo.getAllocateCpuPercentage()));
    allocatedMemoryPercentage
        .setText(String.valueOf(clientInfo.getAllocateMemoryPercentage()));
    nodeId.setText(clientInfo.getNodeId());
    allocatedCpuPercentage.setDisable(false);
    allocatedMemoryPercentage.setDisable(false);
    updateButton.setDisable(false);
    leaveButton.setDisable(false);
    progressIndicator.setVisible(false);
    statusText.setText(
        "This node is ready to use. Check the Service and Status Tabs.");
    tabPaneManager.disableServiceTab(false);
    tabPaneManager.disableStatusTab(false);
  }

  @FXML
  public void onJoinClicked(MouseEvent event) {
    String allocatedMemory = allocatedMemoryPercentage.getText();
    String allocatedCpu = allocatedCpuPercentage.getText();
    logger.info(
        "Join button clicked with parameters allocatedMemoryPercentage = {} and allocatedCpuPercentage = {}",
        allocatedMemory, allocatedCpu);
    Task<ClientInfo> task = new Task<ClientInfo>() {

      protected ClientInfo call() throws Exception {
        return swarmManager.joinSwarm(Integer.parseInt(allocatedMemory),
            Integer.parseInt(allocatedCpu));
      };
    };

    task.setOnSucceeded(e -> {
      ClientInfo clientInfo = task.getValue();
      logger.info("Successfully joined the Swarm {}", clientInfo);
      statusText.setText(
          "Sucessfully joined the Swarm. Check the Service and Status Tabs.");
      allocatedMemoryPercentage
          .setText(String.valueOf(clientInfo.getAllocateMemoryPercentage()));
      allocatedCpuPercentage
          .setText(String.valueOf(clientInfo.getAllocateCpuPercentage()));
      nodeId.setText(clientInfo.getNodeId());
      tabPaneManager.disableServiceTab(false);
      tabPaneManager.disableStatusTab(false);
      allocatedCpuPercentage.setDisable(false);
      allocatedMemoryPercentage.setDisable(false);
      updateButton.setDisable(false);
      leaveButton.setDisable(false);
      progressIndicator.setVisible(false);
    });

    task.setOnFailed(e -> {
      logger.error("Could not join the swarm", task.getException());
      statusText.setText(
          String.format("Could not join the swarm - %s", task.getException()));
    });

    Thread thread = new Thread(task);
    thread.setDaemon(true);
    thread.start();
  }

  @FXML
  public void onUpdateClicked(MouseEvent event) {
    String allocatedMemory = allocatedMemoryPercentage.getText();
    String allocatedCpu = allocatedCpuPercentage.getText();
    logger.info(
        "Update button clicked with parameters allocatedMemoryPercentage = {} and allocatedCpuPercentage = {}",
        allocatedMemory, allocatedCpu);
    progressIndicator.setVisible(true);
    leaveButton.setDisable(true);
    joinButton.setDisable(true);
    updateButton.setDisable(true);
    tabPaneManager.disableServiceTab(true);
    tabPaneManager.disableStatusTab(true);
    allocatedCpuPercentage.setDisable(true);
    allocatedMemoryPercentage.setDisable(true);
    Task<ClientInfo> task = new Task<ClientInfo>() {

      @Override
      protected ClientInfo call() throws Exception {
        Thread.sleep(2000);
        return swarmManager.updateClientInfo(Integer.parseInt(allocatedMemory),
            Integer.parseInt(allocatedCpu));
      }
    };

    task.setOnSucceeded(e -> {
      ClientInfo clientInfo = task.getValue();
      logger.info("Successfully updated the client configuration {}",
          clientInfo);
      statusText.setText("Successfully updated the client configuration");
      allocatedMemoryPercentage
          .setText(String.valueOf(clientInfo.getAllocateMemoryPercentage()));
      allocatedCpuPercentage
          .setText(String.valueOf(clientInfo.getAllocateCpuPercentage()));
      tabPaneManager.disableServiceTab(false);
      tabPaneManager.disableStatusTab(false);
      updateButton.setDisable(false);
      leaveButton.setDisable(false);
      allocatedCpuPercentage.setDisable(false);
      allocatedMemoryPercentage.setDisable(false);
      progressIndicator.setVisible(false);
    });

    task.setOnFailed(e -> {
      logger.error("Something went wrong while updating the configuration",
          task.getException());
      statusText.setText(
          String.format("Something went wrong updating the configuration - %s",
              task.getException()));
      tabPaneManager.disableServiceTab(false);
      tabPaneManager.disableStatusTab(false);
      updateButton.setDisable(false);
      leaveButton.setDisable(false);
      allocatedCpuPercentage.setDisable(false);
      allocatedMemoryPercentage.setDisable(false);
      progressIndicator.setVisible(false);
    });

    Thread thread = new Thread(task);
    thread.setDaemon(true);
    thread.start();
  }

  @FXML
  public void onLeaveClicked(MouseEvent event)
      throws DockerException, InterruptedException {
    logger.info("Leave button clicked");
    progressIndicator.setVisible(true);
    leaveButton.setDisable(true);
    joinButton.setDisable(true);
    updateButton.setDisable(true);
    tabPaneManager.disableServiceTab(true);
    tabPaneManager.disableStatusTab(true);
    allocatedCpuPercentage.setDisable(true);
    allocatedMemoryPercentage.setDisable(true);
    Task<Void> task = new Task<Void>() {

      @Override
      protected Void call() throws Exception {
        swarmManager.leaveSwarm();
        return null;
      }
    };
    task.setOnSucceeded(e -> {
      logger.info("Left the swarm successfully");
      tabPaneManager.disableServiceTab(true);
      tabPaneManager.disableStatusTab(true);
      statusText.setText("");
      joinButton.setDisable(false);
      progressIndicator.setVisible(false);
      allocatedCpuPercentage.setDisable(false);
      allocatedMemoryPercentage.setDisable(false);
      nodeId.setText("");
    });

    task.setOnFailed(e -> {
      logger.error("Something went wrong while leaving the swarm",
          task.getException());
      statusText.setText(
          String.format("Something went wrong while leaving the swarm - %s",
              task.getException()));
      tabPaneManager.disableServiceTab(false);
      tabPaneManager.disableStatusTab(false);
      updateButton.setDisable(false);
      leaveButton.setDisable(false);
      allocatedCpuPercentage.setDisable(false);
      allocatedMemoryPercentage.setDisable(false);
      progressIndicator.setVisible(false);
    });
    Thread thread = new Thread(task);
    thread.setDaemon(true);
    thread.start();
  }

}
