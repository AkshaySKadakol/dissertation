package com.dissertation.bits.client.model;

import java.io.Serializable;

public class PortMapping implements Serializable {

  private static final long serialVersionUID = 7025012014421817896L;

  private int publishedPort;

  private int targetPort;

  public PortMapping() {}

  public PortMapping(int publishedPort, int targetPort) {
    this.publishedPort = publishedPort;
    this.targetPort = targetPort;
  }

  public int getPublishedPort() {
    return publishedPort;
  }

  public int getTargetPort() {
    return targetPort;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("class PortMapping {\n");
    sb.append("  publishedPort: ").append(publishedPort).append('\n');
    sb.append("  targetPort: ").append(targetPort).append('\n');
    sb.append("}\n");
    return sb.toString();
  }
}
