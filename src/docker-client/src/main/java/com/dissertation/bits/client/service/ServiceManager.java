package com.dissertation.bits.client.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.dissertation.bits.client.model.PortMapping;
import com.dissertation.bits.client.model.ServiceInfo;
import com.dissertation.bits.client.model.Task;
import com.spotify.docker.client.exceptions.DockerException;

@Service
public class ServiceManager extends DockerService {

  private static final Logger logger =
      LoggerFactory.getLogger(DockerService.class);

  @Autowired
  private RestTemplate restTemplate;

  protected ServiceManager() {}

  public void tagAndPush(String imageName)
      throws DockerException, InterruptedException {
    String taggedName;
    if (imageName.startsWith("localhost") || imageName.startsWith("10.")) {
      taggedName = String.format("%s/%s", super.getRegistry(),
          imageName.substring(imageName.indexOf('/') + 1));
    } else {
      taggedName = String.format("%s/%s", super.getRegistry(), imageName);
    }
    logger.info("Tag the image {} as {} and push to registry {}", imageName,
        taggedName, super.getRegistry());
    super.getDockerClient().tag(imageName, taggedName);
    super.getDockerClient().push(taggedName);
  }

  public boolean createService(String imageName, String allocatedMemory,
      String allocatedCpu, String cmd, String scale, String targetPort,
      String publishPort, String serviceName)
      throws DockerException, InterruptedException {
    String nodeId = super.getDockerInfo().swarm().nodeId();
    String url =
        String.format("%s/nodes/%s/services", super.getServerUrl(), nodeId);
    ServiceInfo serviceInfo = new ServiceInfo();
    serviceInfo.setImageName(imageName);
    serviceInfo.setAllocatedMemory(Integer.parseInt(allocatedMemory));
    serviceInfo.setAllocatedCpu(Float.parseFloat(allocatedCpu));
    serviceInfo.setCmd(cmd.split(" "));
    serviceInfo.setScale(Integer.parseInt(scale));
    if (publishPort != null && targetPort != null && !publishPort.isEmpty()
        && !targetPort.isEmpty()) {
      serviceInfo.setPortMapping(new PortMapping(Integer.parseInt(publishPort),
          Integer.parseInt(targetPort)));
    }
    serviceInfo.setServiceName(serviceName);
    HttpEntity<ServiceInfo> httpEntity =
        new HttpEntity<ServiceInfo>(serviceInfo);
    restTemplate.exchange(url, HttpMethod.POST, httpEntity, ServiceInfo.class);
    return true;
  }

  public ServiceInfo[] getServiceList()
      throws DockerException, InterruptedException {
    String nodeId = super.getDockerInfo().swarm().nodeId();
    String url =
        String.format("%s/nodes/%s/services", super.getServerUrl(), nodeId);
    return restTemplate.getForEntity(url, ServiceInfo[].class).getBody();
  }

  public Task[] getTasksForServiceName(String serviceName)
      throws DockerException, InterruptedException {
    String nodeId = super.getDockerInfo().swarm().nodeId();
    String url = String.format("%s/nodes/%s/services/%s/tasks",
        super.getServerUrl(), nodeId, serviceName);
    return restTemplate.getForEntity(url, Task[].class).getBody();
  }

  public ServiceInfo updateService(ServiceInfo serviceInfo)
      throws DockerException, InterruptedException {
    logger.info("Updating service {}", serviceInfo);
    String nodeId = super.getDockerInfo().swarm().nodeId();
    String url = String.format("%s/nodes/%s/services/%s", super.getServerUrl(),
        nodeId, serviceInfo.getServiceName());
    HttpEntity<ServiceInfo> httpEntity =
        new HttpEntity<ServiceInfo>(serviceInfo);
    return restTemplate
        .exchange(url, HttpMethod.PUT, httpEntity, ServiceInfo.class).getBody();
  }

  public boolean deleteService(ServiceInfo serviceInfo)
      throws DockerException, InterruptedException {
    String nodeId = super.getDockerInfo().swarm().nodeId();
    String url = String.format("%s/nodes/%s/services/%s", super.getServerUrl(),
        nodeId, serviceInfo.getServiceName());
    restTemplate.delete(url);
    return true;
  }

}
