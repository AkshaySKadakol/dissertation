package com.dissertation.bits.client.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spotify.docker.client.exceptions.DockerException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

@Component
public class TabPaneManager {

  private static final Logger logger =
      LoggerFactory.getLogger(TabPaneManager.class);

  private final SwarmTabController swarmTabController;
  private final ServiceTabController serviceTabController;
  private final StatusTabController statusTabController;

  @FXML
  private TabPane tabPane;

  @FXML
  private Tab swarmTab;

  @FXML
  private Tab serviceTab;

  @FXML
  private Tab statusTab;

  @Autowired
  public TabPaneManager(SwarmTabController swarmTabController,
      ServiceTabController serviceTabController,
      StatusTabController statusTabController) {
    this.swarmTabController = swarmTabController;
    this.serviceTabController = serviceTabController;
    this.statusTabController = statusTabController;
    swarmTabController.setTabPaneManager(this);
    serviceTabController.setTabPaneManager(this);
    statusTabController.setTabPaneManager(this);
  }

  public void initialize() {
    tabPane.getSelectionModel().selectedItemProperty()
        .addListener(new ChangeListener<Tab>() {

          @Override
          public void changed(ObservableValue<? extends Tab> observable,
              Tab oldValue, Tab newValue) {
            logger.debug("Tab changed from old = {} to New  = {}",
                oldValue.getId(), newValue.getId());
            switch (newValue.getId()) {
              case "swarmTab":
                break;
              case "statusTab":
                statusTabController.setup();
                break;
              case "serviceTab":
                try {
                  serviceTabController.setup();
                } catch (DockerException | InterruptedException e) {
                  logger.error("Exception when switching to serviceTab", e);
                }
                break;
              default:
                break;
            }
          }
        });
  }

  public void disableSwarmTab(boolean value) {
    swarmTab.setDisable(value);
  }

  public void disableServiceTab(boolean value) {
    serviceTab.setDisable(value);
  }

  public void disableStatusTab(boolean value) {
    statusTab.setDisable(value);
  }

}
