package com.dissertation.bits.client.service;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.dissertation.bits.client.model.ClientInfo;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.swarm.SwarmInfo;
import com.spotify.docker.client.messages.swarm.SwarmJoin;

@Service
public class SwarmManager extends DockerService {

  private static final Logger logger =
      LoggerFactory.getLogger(SwarmManager.class);

  @Autowired
  private RestTemplate restTemplate;

  protected SwarmManager() {}

  public SwarmInfo getSwarmInfo() throws DockerException, InterruptedException {
    return super.getDockerClient().info().swarm();
  }

  public ClientInfo getClientInfo()
      throws DockerException, InterruptedException {
    String nodeId = super.getDockerInfo().swarm().nodeId();
    String url =
        String.format("%s/clients/node/%s", super.getServerUrl(), nodeId);
    logger.info("Connecting to url {}", url);
    return restTemplate.getForEntity(url, ClientInfo.class).getBody();
  }

  public void leaveSwarm() throws DockerException, InterruptedException {
    String nodeId = super.getDockerInfo().swarm().nodeId();
    String url =
        String.format("%s/clients/node/%s", super.getServerUrl(), nodeId);
    logger.info("Connecting to url {}", url);
    restTemplate.delete(url);

    super.getDockerClient().leaveSwarm(true);
  }

  public ClientInfo updateClientInfo(int allocatedMemory, int allocatedCpu)
      throws DockerException, InterruptedException {
    String nodeId = super.getDockerInfo().swarm().nodeId();
    ClientInfo clientInfo = new ClientInfo();
    clientInfo.setAllocateCpuPercentage(allocatedCpu);
    clientInfo.setAllocateMemoryPercentage(allocatedMemory);
    String url =
        String.format("%s/clients/node/%s", super.getServerUrl(), nodeId);
    logger.info("Connecting to url {}", url);
    HttpEntity<ClientInfo> httpEntity = new HttpEntity<ClientInfo>(clientInfo);
    return restTemplate
        .exchange(url, HttpMethod.PUT, httpEntity, ClientInfo.class).getBody();
  }

  public ClientInfo joinSwarm(int allocatedMemory, int allocatedCpu)
      throws DockerException, InterruptedException {
    getTokenAndJoinDockerSwarm();
    return postClientInfo(allocatedMemory, allocatedCpu);
  }

  private void getTokenAndJoinDockerSwarm()
      throws DockerException, InterruptedException {
    String url = String.format("%s/swarm/worker-token", super.getServerUrl());
    logger.info("Connecting to url {}", url);
    String workerToken = restTemplate.getForEntity(url, String.class).getBody();
    logger.info("Retrieved worker token {}", workerToken);

    super.getDockerClient()
        .joinSwarm(
            SwarmJoin.builder().listenAddr("0.0.0.0:2377")
                .remoteAddrs(Arrays
                    .asList(String.format("%s:2377", super.getServerIp())))
                .joinToken(workerToken).build());
  }

  private ClientInfo postClientInfo(int allocatedMemory, int allocatedCpu)
      throws DockerException, InterruptedException {
    String nodeId = super.getDockerInfo().swarm().nodeId();
    ClientInfo clientInfo = new ClientInfo();
    clientInfo.setNodeId(nodeId);
    clientInfo.setAllocateCpuPercentage(allocatedCpu);
    clientInfo.setAllocateMemoryPercentage(allocatedMemory);
    String url = String.format("%s/clients", super.getServerUrl());
    logger.info("Connecting to url {}", url);
    return restTemplate.postForEntity(url, clientInfo, ClientInfo.class)
        .getBody();
  }

}
