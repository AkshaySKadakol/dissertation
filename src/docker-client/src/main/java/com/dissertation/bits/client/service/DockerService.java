package com.dissertation.bits.client.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Image;
import com.spotify.docker.client.messages.Info;

@Service
public class DockerService {

  private static final Logger logger =
      LoggerFactory.getLogger(DockerService.class);

  @Value("${dockerUri}")
  private String dockerUri;

  private DockerClient dockerClient;

  @Value("${registry}")
  private String registry;

  @Value("${serverIp}")
  private String serverIp;

  @Value("${serverPort}")
  private String serverPort;

  private String serverUrl;

  protected DockerService() {}

  protected DockerClient getDockerClient() {
    return dockerClient;
  }

  public String getRegistry() {
    return registry;
  }

  public String getServerIp() {
    return serverIp;
  }

  public String getServerPort() {
    return serverPort;
  }

  public String getServerUrl() {
    return serverUrl;
  }

  @PostConstruct
  private void setup() throws DockerException, InterruptedException {
    dockerClient = DefaultDockerClient.builder().uri(dockerUri).build();
    serverUrl = String.format("http://%s:%s", serverIp, serverPort);
    logger.info(
        "Started with parameters serverIp = {}, serverPort = {}, serverUrl = {}, registry = {}",
        serverIp, serverPort, serverUrl, registry);
  }

  @PreDestroy
  private void shutdown() {
    dockerClient.close();
  }

  public Info getDockerInfo() throws DockerException, InterruptedException {
    return dockerClient.info();
  }

  public List<Image> listImages() throws DockerException, InterruptedException {
    return dockerClient.listImages();
  }
}
