package com.dissertation.bits.client.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dissertation.bits.client.service.DockerService;
import com.dissertation.bits.client.service.ServiceManager;
import com.spotify.docker.client.exceptions.DockerException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

@Component
public class ServiceTabController {

  private static final Logger logger =
      LoggerFactory.getLogger(ServiceTabController.class);

  private final DockerService dockerService;
  private final ServiceManager serviceManager;

  @FXML
  private ListView<String> serviceList;

  @FXML
  private TextField allocatedMemory, allocatedCpu, cmd, scale, targetPort,
      publishPort, serviceName;

  @FXML
  private Button submitButton, refreshButton;

  @FXML
  private ProgressIndicator progressIndicator;

  @FXML
  private Text submissionStatus;

  private TabPaneManager tabPaneManager;

  @Autowired
  public ServiceTabController(DockerService dockerService,
      ServiceManager serviceManager) {
    this.dockerService = dockerService;
    this.serviceManager = serviceManager;
  }

  public void setTabPaneManager(TabPaneManager tabPaneManager) {
    this.tabPaneManager = tabPaneManager;
  }

  public void setup() throws DockerException, InterruptedException {
    submitButton.setDisable(true);
    refreshButton.setDisable(true);
    serviceList.getItems().clear();
    submissionStatus.setText("");
    tabPaneManager.disableStatusTab(true);
    tabPaneManager.disableSwarmTab(true);
    Task<ObservableList<String>> task = new Task<ObservableList<String>>() {

      @Override
      protected ObservableList<String> call() throws Exception {
        return FXCollections.observableList(getDockerImageNamesAsList());
      }
    };

    task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

      @Override
      public void handle(WorkerStateEvent event) {
        serviceList.setItems(task.getValue());
        serviceList.getSelectionModel().select(0);
        submitButton.setDisable(false);
        refreshButton.setDisable(false);
        tabPaneManager.disableStatusTab(false);
        tabPaneManager.disableSwarmTab(false);
      }
    });

    task.setOnFailed(event -> {
      logger.error("Something went wrong while loading the images",
          task.getException());
    });

    Thread thread = new Thread(task);
    thread.setDaemon(true);
    thread.start();
  }

  private List<String> getDockerImageNamesAsList()
      throws DockerException, InterruptedException {
    List<String> imageList = new ArrayList<>();
    dockerService.listImages().forEach(image -> {
      try {
        image.repoTags().forEach(imageList::add);
      } catch (Exception e) {
        logger.error("Could not add this image to the list {}", image, e);
      }
    });
    return imageList;
  }

  @FXML
  public void onRefreshButtonClicked(MouseEvent event)
      throws DockerException, InterruptedException {
    setup();
  }

  @FXML
  public void onSubmitClicked(MouseEvent event) {
    String imageName = serviceList.getSelectionModel().getSelectedItem();
    String allocatedMemoryValue = allocatedMemory.getText();
    String allocatedCpuValue = allocatedCpu.getText();
    String command = cmd.getText();
    String scaleValue = scale.getText();
    String targetPortValue = targetPort.getText();
    String publishPortValue = publishPort.getText();
    String serviceNameValue = serviceName.getText();
    logger.info(
        "Submit button clicked with parameters imageName = {}, allocatedMemory = {}, allocatedCpu = {}, cmd = {}, scale = {}, targetPort = {}, publishPort = {}, serviceName = {}",
        imageName, allocatedMemoryValue, allocatedCpuValue, command, scaleValue,
        targetPortValue, publishPortValue, serviceNameValue);
    submitButton.setDisable(true);
    refreshButton.setDisable(true);
    progressIndicator.setVisible(true);
    submissionStatus.setVisible(false);
    tabPaneManager.disableStatusTab(true);
    tabPaneManager.disableSwarmTab(true);
    Task<Boolean> task = new Task<Boolean>() {

      @Override
      protected Boolean call() throws Exception {
        serviceManager.tagAndPush(imageName);
        return serviceManager.createService(imageName, allocatedMemoryValue,
            allocatedCpuValue, command, scaleValue, targetPortValue,
            publishPortValue, serviceNameValue);
      }
    };

    task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

      @Override
      public void handle(WorkerStateEvent event) {
        if (task.getValue()) {
          submissionStatus.setText(
              "Successfully created the service. Please check the Status tab.");
        } else {
          submissionStatus
              .setText("Something went wrong while creating the service");
        }
        submissionStatus.setVisible(true);
        progressIndicator.setVisible(false);
        submitButton.setDisable(false);
        refreshButton.setDisable(false);
        tabPaneManager.disableStatusTab(false);
        tabPaneManager.disableSwarmTab(false);
      }
    });

    task.setOnFailed(new EventHandler<WorkerStateEvent>() {

      @Override
      public void handle(WorkerStateEvent event) {
        logger.error("Failed to create service", task.getException());
        submissionStatus.setVisible(true);
        submissionStatus
            .setText("Something went wrong while creating the service. "
                + task.getException().getMessage());
        submitButton.setDisable(false);
        refreshButton.setDisable(false);
        progressIndicator.setVisible(false);
        tabPaneManager.disableStatusTab(false);
        tabPaneManager.disableSwarmTab(false);
      }
    });

    Thread thread = new Thread(task);
    thread.setDaemon(true);
    thread.start();
  }

}
