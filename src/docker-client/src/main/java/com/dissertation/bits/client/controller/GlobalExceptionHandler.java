package com.dissertation.bits.client.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

@ControllerAdvice
public class GlobalExceptionHandler {

  private static final Logger logger =
      LoggerFactory.getLogger(GlobalExceptionHandler.class);

  @ExceptionHandler(Exception.class)
  public void exception(Exception exception) {
    logger.error("Exception handled", exception);
    Stage stage = new Stage();
    stage.setHeight(200);
    stage.setWidth(400);
    TextField message =
        new TextField("Something went wrong" + exception.getStackTrace());
    stage.setScene(new Scene(message));
    stage.show();
  }

  @ExceptionHandler(HttpClientErrorException.class)
  public void exception(HttpClientErrorException exception) {
    logger.error("HttpClientErrorException handled", exception);
    Stage stage = new Stage();
    stage.setHeight(200);
    stage.setWidth(400);
    TextField message = new TextField(
        "Something went wrong while communicating with an external Service"
            + exception.getStackTrace());
    stage.setScene(new Scene(message));
    stage.show();
  }
}
