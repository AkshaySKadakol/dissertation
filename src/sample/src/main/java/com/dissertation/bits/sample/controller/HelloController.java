package com.dissertation.bits.sample.controller;

import java.util.UUID;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "hello")
public class HelloController {

  private static final Logger logger =
      LoggerFactory.getLogger(HelloController.class);

  private String hello;

  @PostConstruct
  private void setup() {
    hello =
        String.format("Hello from instance %s", UUID.randomUUID().toString());
  }

  @RequestMapping(value = "", method = RequestMethod.GET)
  public String hello() {
    logger.info("Get hello method called. Returning {}", hello);
    return hello;
  }
}
