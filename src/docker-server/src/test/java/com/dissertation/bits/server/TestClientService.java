package com.dissertation.bits.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.dissertation.bits.server.model.ClientInfo;
import com.dissertation.bits.server.service.ClientService;
import com.dissertation.bits.server.service.InvalidValuesException;
import com.spotify.docker.client.exceptions.DockerException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestClientService {

  @Autowired
  private ClientService clientService;

  @Test(expected = InvalidValuesException.class)
  public void invalidClientInfoThrowsException()
      throws DockerException, InterruptedException {
    clientService.validateClientInfo(new ClientInfo());
  }
}
