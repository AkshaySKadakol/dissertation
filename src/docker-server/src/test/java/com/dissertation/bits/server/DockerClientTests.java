package com.dissertation.bits.server;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.swarm.ContainerSpec;
import com.spotify.docker.client.messages.swarm.EndpointSpec;
import com.spotify.docker.client.messages.swarm.Placement;
import com.spotify.docker.client.messages.swarm.PortConfig;
import com.spotify.docker.client.messages.swarm.Service;
import com.spotify.docker.client.messages.swarm.Service.Criteria;
import com.spotify.docker.client.messages.swarm.ServiceMode;
import com.spotify.docker.client.messages.swarm.ServiceSpec;
import com.spotify.docker.client.messages.swarm.Swarm;
import com.spotify.docker.client.messages.swarm.SwarmInit;
import com.spotify.docker.client.messages.swarm.SwarmJoin;
import com.spotify.docker.client.messages.swarm.Task;
import com.spotify.docker.client.messages.swarm.TaskSpec;

public class DockerClientTests {

  private DockerClient dockerClient;

  @Before
  public void setup() throws DockerCertificateException {
    dockerClient = DefaultDockerClient.builder()
        .uri("unix:///var/run/docker.sock").build();
  }

  @Test
  public void listContainers() throws DockerException, InterruptedException {
    List<Container> listContainers = dockerClient.listContainers();
    for (Container container : listContainers) {
      System.out.println("Container name = " + container.names());
    }
  }

  // @Test
  public void initSwarm() throws DockerException, InterruptedException {
    // String ip = "10.170.5.184";
    String ip = "192.168.1.108";
    dockerClient.initSwarm(
        SwarmInit.builder().listenAddr(ip).advertiseAddr(ip).build());
    Swarm swarm = dockerClient.inspectSwarm();
    System.out.println("Swarm = " + swarm);
  }

  // @Test
  public void inspectSwarm() throws DockerException, InterruptedException {
    Swarm swarm = dockerClient.inspectSwarm();
    System.out.println("Swarm = " + swarm);
    dockerClient.listNodes()
        .forEach(node -> System.out.println("Node = " + node));
  }

  @Test
  public void listImages() throws DockerException, InterruptedException {
    dockerClient.listImages()
        .forEach(image -> System.out.println(image.repoTags()));
  }

  // @Test
  public void tagAndPushImage() throws DockerException, InterruptedException {
    dockerClient.tag("alpine", "10.170.5.184:5000/docker-alpine");
    dockerClient.push("10.170.5.184:5000/docker-alpine");
    dockerClient.listImages()
        .forEach(image -> System.out.println(image.repoTags()));
  }

  // @Test
  public void scaleService() throws DockerException, InterruptedException {
    createService("spotify-docker");
    List<Service> services = dockerClient
        .listServices(Criteria.builder().serviceName("spotify-docker").build());
    Service service = services.get(0);
    dockerClient.updateService(service.id(), service.version().index(),
        ServiceSpec.builder().name(service.spec().name())//
            .taskTemplate(service.spec().taskTemplate())//
            .endpointSpec(service.spec().endpointSpec())//
            .updateConfig(service.spec().updateConfig())//
            .mode(ServiceMode.withReplicas(5))//
            .build());
    dockerClient.listServices()
        .forEach(s -> System.out.println("Service = " + s));
  }

  // @Test
  public void createService() throws DockerException, InterruptedException {
    createService("nginx");
    dockerClient.listServices()
        .forEach(service -> System.out.println("Services = " + service));
  }

  private void createService(String name)
      throws DockerException, InterruptedException {
    String image = "nginx";
    dockerClient.createService(ServiceSpec.builder().name(name)
        .endpointSpec(EndpointSpec.builder()
            .addPort(PortConfig.builder().name("web").protocol("tcp")
                .publishedPort(10001).targetPort(80).build())
            .build())
        .taskTemplate(TaskSpec.builder()
            .containerSpec(ContainerSpec.builder().image(image).build())
            .placement(Placement.create(Arrays
                .asList(String.format("node.id == rm0ufefjjol5z36hyxoc0csaq"))))
            .build())
        .build());
  }

  // @Test
  public void deleteService() throws DockerException, InterruptedException {
    if (dockerClient
        .listServices(Criteria.builder().serviceName("docker-spotify").build())
        .size() == 0) {
      createService("spotify-docker");
    }
    dockerClient.removeService("spotify-docker");
    dockerClient.listServices()
        .forEach(s -> System.out.println("Service = " + s));
    Assert.assertEquals(dockerClient.listServices(
        Criteria.builder().serviceName("docker-spotify").build()), 0);
  }

  // @Test
  public void destroySwarm() throws DockerException, InterruptedException {
    dockerClient.leaveSwarm(true);
  }

  @Test
  public void getDockerInfo() throws DockerException, InterruptedException {
    System.out.println(dockerClient.info());
  }

  // @Test
  public void joinSwarm() throws DockerException, InterruptedException {
    dockerClient.joinSwarm(SwarmJoin.builder().listenAddr("0.0.0.0:2377")
        .remoteAddrs(Arrays.asList("10.170.7.121:2377"))
        .joinToken(
            "SWMTKN-1-5f05gm5xf5sydcbwyzg8rdw14gjemaun3hqq2i12v1bak3rnjv-9yno8e8bvkdr3ldodajguafya")
        .build());
  }

  @After
  public void cleanup() {
    dockerClient.close();
  }

  // @Test
  public void listServices() throws DockerException, InterruptedException {
    // List<Service> services = dockerClient.listServices(Criteria.builder()
    // .serviceName("packer_r5vcgcrajybp0qehyih9i4a4p").build());
    // dockerClient.listServices().forEach(System.out::println);
    dockerClient.listTasks(Task.Criteria.builder().serviceName("test5").build())
        .forEach(task -> System.out.println("\n" + task + "\n"));
  }

}

