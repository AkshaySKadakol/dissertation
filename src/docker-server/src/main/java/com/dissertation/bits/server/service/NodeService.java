package com.dissertation.bits.server.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dissertation.bits.server.model.ServiceInfo;
import com.dissertation.bits.server.repository.EntityExistsException;
import com.dissertation.bits.server.repository.EntityNotFoundException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ServiceCreateResponse;
import com.spotify.docker.client.messages.swarm.ContainerSpec;
import com.spotify.docker.client.messages.swarm.EndpointSpec;
import com.spotify.docker.client.messages.swarm.PortConfig;
import com.spotify.docker.client.messages.swarm.ResourceRequirements;
import com.spotify.docker.client.messages.swarm.Resources;
import com.spotify.docker.client.messages.swarm.Service;
import com.spotify.docker.client.messages.swarm.Service.Criteria;
import com.spotify.docker.client.messages.swarm.ServiceMode;
import com.spotify.docker.client.messages.swarm.ServiceSpec;
import com.spotify.docker.client.messages.swarm.Task;
import com.spotify.docker.client.messages.swarm.TaskSpec;

@org.springframework.stereotype.Service
public class NodeService extends DockerService {

  private static final Logger logger =
      LoggerFactory.getLogger(NodeService.class);

  protected NodeService() {}

  public boolean validateServiceInfo(ServiceInfo serviceInfo)
      throws DockerException, InterruptedException {
    if (serviceInfo.getServiceName() == null
        || serviceInfo.getServiceName().isEmpty()) {
      throw new InvalidValuesException("Service name cannot be empty",
          "serviceName");
    }
    Service service = super.getDockerClient()
        .listServices(Criteria.builder()
            .serviceName(serviceInfo.getServiceName()).build())
        .stream().findFirst().orElse(null);
    if (service != null) {
      logger.info("Found a service with the given name {}",
          service.spec().name());
      throw new EntityExistsException(
          "A service with the given name already exists. Please use a different name.",
          "serviceName");
    }
    if (serviceInfo.getAllocatedCpu() == 0) {
      throw new InvalidValuesException("CPU allocated cannot be 0",
          "allocatedCpu");
    }
    if (serviceInfo.getAllocatedMemory() == 0) {
      throw new InvalidValuesException("Memory allocated cannot be 0",
          "allocatedMemory");
    }
    if (serviceInfo.getScale() == 0) {
      throw new InvalidValuesException("Scale cannot be 0", "scale");
    }
    if (serviceInfo.getImageName() == null
        || serviceInfo.getImageName().isEmpty()) {
      throw new InvalidValuesException("Image name cannot be empty",
          "imageName");
    }
    return true;
  }

  public ServiceInfo createService(ServiceInfo serviceInfo)
      throws DockerException, InterruptedException {
    long memoryBytes = serviceInfo.getAllocatedMemory() * 1024 * 1024;
    long nanoCpus = (long) (serviceInfo.getAllocatedCpu() * Math.pow(10, 9));
    Resources resources =
        Resources.builder().memoryBytes(memoryBytes).nanoCpus(nanoCpus).build();
    ContainerSpec.Builder containerSpecBuilder = ContainerSpec.builder();
    containerSpecBuilder.image(serviceInfo.getImageName());
    if (serviceInfo.getCmd() != null && serviceInfo.getCmd().length != 0
        && serviceInfo.getCmd()[0].length() != 0) {
      logger.info("Creating a service with the following cmd - {}, length - {}",
          serviceInfo.getCmd()[0], serviceInfo.getCmd().length);
      containerSpecBuilder.command(serviceInfo.getCmd());
    }
    ContainerSpec containerSpec = containerSpecBuilder.build();
    TaskSpec taskSpec = TaskSpec
        .builder().containerSpec(containerSpec).resources(ResourceRequirements
            .builder().limits(resources).reservations(resources).build())
        .build();
    ServiceSpec.Builder serviceSpecBuilder = ServiceSpec.builder()
        .name(serviceInfo.getServiceName()).taskTemplate(taskSpec)
        .mode(ServiceMode.withReplicas(serviceInfo.getScale()));
    if (serviceInfo.getPortMapping() != null) {
      serviceSpecBuilder.endpointSpec(EndpointSpec.builder()
          .addPort(PortConfig.builder().name("web").protocol("tcp")
              .publishedPort(serviceInfo.getPortMapping().getPublishedPort())
              .targetPort(serviceInfo.getPortMapping().getTargetPort()).build())
          .build());
    }
    ServiceSpec serviceSpec = serviceSpecBuilder.build();
    logger.info("Creating a service with the following specs - {}",
        serviceSpec);
    ServiceCreateResponse serviceCreateResponse =
        super.getDockerClient().createService(serviceSpec);
    serviceInfo.setServiceId(serviceCreateResponse.id());
    return serviceInfo;
  }

  public void deleteService(String serviceName)
      throws DockerException, InterruptedException {
    Service service = getServiceByName(serviceName);
    super.getDockerClient().removeService(service.id());
  }

  private Service getServiceByName(String serviceName)
      throws DockerException, InterruptedException {
    Service service = super.getDockerClient()
        .listServices(Criteria.builder().serviceName(serviceName).build())
        .stream().findFirst().orElse(null);
    if (service == null) {
      throw new EntityNotFoundException(
          "A service with the given name does not exist.", "serviceName");
    }
    return service;
  }

  public List<Task> getTasksForService(String serviceName)
      throws DockerException, InterruptedException {
    getServiceByName(serviceName);
    return super.getDockerClient()
        .listTasks(Task.Criteria.builder().serviceName(serviceName).build());
  }

  public void updateService(ServiceInfo existingServiceInfo,
      ServiceInfo newServiceInfo) throws DockerException, InterruptedException {
    Service service = getServiceByName(existingServiceInfo.getServiceName());
    Resources.Builder resourceBuilder = Resources.builder();
    if (newServiceInfo.getAllocatedCpu() != 0) {
      long nanoCpus =
          (long) (newServiceInfo.getAllocatedCpu() * Math.pow(10, 9));
      resourceBuilder.nanoCpus(nanoCpus);
      existingServiceInfo.setAllocatedCpu(newServiceInfo.getAllocatedCpu());
    } else {
      long nanoCpus =
          (long) (existingServiceInfo.getAllocatedCpu() * Math.pow(10, 9));
      resourceBuilder.nanoCpus(nanoCpus);
    }
    if (newServiceInfo.getAllocatedMemory() != 0) {
      long memoryBytes = newServiceInfo.getAllocatedMemory() * 1024 * 1024;
      resourceBuilder.memoryBytes(memoryBytes);
      existingServiceInfo
          .setAllocatedMemory(newServiceInfo.getAllocatedMemory());
    } else {
      long memoryBytes = existingServiceInfo.getAllocatedMemory() * 1024 * 1024;
      resourceBuilder.memoryBytes(memoryBytes);
    }
    Resources resources = resourceBuilder.build();
    TaskSpec taskTemplate = service.spec().taskTemplate();
    TaskSpec taskSpec =
        TaskSpec.builder().containerSpec(taskTemplate.containerSpec())
            .resources(ResourceRequirements.builder().limits(resources)
                .reservations(resources).build())
            .build();
    ServiceSpec.Builder serviceSpecBuilder =
        ServiceSpec.builder().name(service.spec().name())//
            .taskTemplate(taskSpec)//
            .endpointSpec(service.spec().endpointSpec())//
            .updateConfig(service.spec().updateConfig());
    if (newServiceInfo.getScale() != 0) {
      serviceSpecBuilder
          .mode(ServiceMode.withReplicas(newServiceInfo.getScale()));
      existingServiceInfo.setScale(newServiceInfo.getScale());
    } else {
      serviceSpecBuilder.mode(ServiceMode
          .withReplicas(service.spec().mode().replicated().replicas()));
    }
    ServiceSpec newServiceSpec = serviceSpecBuilder.build();
    logger.info("Updating the service with the following specs {}",
        newServiceSpec);
    super.getDockerClient().updateService(service.id(),
        service.version().index(), newServiceSpec);
  }

}
