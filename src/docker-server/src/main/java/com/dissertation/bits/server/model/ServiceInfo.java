package com.dissertation.bits.server.model;

import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "services")
public class ServiceInfo {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @NotNull
  @ManyToOne
  private ClientInfo clientInfo;

  @NotNull
  private String serviceId;

  @NotNull
  private String imageName;

  private String[] cmd;

  @NotNull
  private int allocatedMemory;

  @NotNull
  private float allocatedCpu;

  @NotNull
  private int scale;

  private PortMapping portMapping;

  @NotNull
  private String serviceName;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public ClientInfo getClientInfo() {
    return clientInfo;
  }

  public void setClientInfo(ClientInfo clientInfo) {
    this.clientInfo = clientInfo;
  }

  public String getServiceId() {
    return serviceId;
  }

  public void setServiceId(String serviceId) {
    this.serviceId = serviceId;
  }

  public String getImageName() {
    return imageName;
  }

  public void setImageName(String imageName) {
    this.imageName = imageName;
  }

  public String[] getCmd() {
    return cmd;
  }

  public void setCmd(String[] cmd) {
    this.cmd = cmd;
  }

  public int getAllocatedMemory() {
    return allocatedMemory;
  }

  public void setAllocatedMemory(int allocatedMemory) {
    this.allocatedMemory = allocatedMemory;
  }

  public float getAllocatedCpu() {
    return allocatedCpu;
  }

  public void setAllocatedCpu(float allocatedCpu) {
    this.allocatedCpu = allocatedCpu;
  }

  public int getScale() {
    return scale;
  }

  public void setScale(int scale) {
    this.scale = scale;
  }

  public PortMapping getPortMapping() {
    return portMapping;
  }

  public void setPortMapping(PortMapping portMapping) {
    this.portMapping = portMapping;
  }

  public String getServiceName() {
    return serviceName;
  }

  public void setServiceName(String serviceName) {
    this.serviceName = serviceName;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("class ServiceInfo {\n");
    sb.append("  id: ").append(id).append('\n');
    sb.append("  clientInfo: ").append(clientInfo).append('\n');
    sb.append("  serviceId: ").append(serviceId).append('\n');
    sb.append("  imageName: ").append(imageName).append('\n');
    sb.append("  cmd: ").append(Arrays.toString(cmd)).append('\n');
    sb.append("  allocatedMemory: ").append(allocatedMemory).append('\n');
    sb.append("  allocatedCpu: ").append(allocatedCpu).append('\n');
    sb.append("  scale: ").append(scale).append('\n');
    sb.append("  portMapping: ").append(portMapping).append('\n');
    sb.append("  serviceName: ").append(serviceName).append('\n');
    sb.append("}\n");
    return sb.toString();
  }

}
