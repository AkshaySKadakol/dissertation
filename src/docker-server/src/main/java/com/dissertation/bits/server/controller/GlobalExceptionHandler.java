package com.dissertation.bits.server.controller;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.dissertation.bits.server.model.Error;
import com.dissertation.bits.server.repository.DataAccessException;
import com.dissertation.bits.server.repository.EntityExistsException;
import com.dissertation.bits.server.repository.EntityNotFoundException;
import com.dissertation.bits.server.service.InvalidValuesException;
import com.dissertation.bits.server.service.ServiceException;
import com.spotify.docker.client.exceptions.DockerException;

/**
 * All exceptions thrown by any Controller will finally end up here. Handle them appropriately all
 * in one place.
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  /** The Constant logbackLogger. */
  private static final Logger logbackLogger =
      LoggerFactory.getLogger(GlobalExceptionHandler.class);

  /**
   * Handle all classes of exceptions and return the message contained in the exception.
   * 
   * @param exception Exception propagated to this class.
   * @return ResponseEntity which will contain an Error object.
   */
  @ExceptionHandler(Exception.class)
  public ResponseEntity<Error> handleException(Exception exception) {
    logbackLogger.error("handleGenericException", exception);
    return new ResponseEntity<Error>(new Error(exception.getMessage(), "None"),
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Handle all HttpRequestMethodNotSupportedExceptions and return the message contained in the
   * exception.
   *
   * @param exception HttpRequestMethodNotSupportedException propagated to this class.
   * @param headers the headers
   * @param status the status
   * @param request the request
   * @return ResponseEntity which will contain an Error object.
   */
  @Override
  protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
      HttpRequestMethodNotSupportedException exception, HttpHeaders headers,
      HttpStatus status, WebRequest request) {
    logbackLogger.error("handleHttpRequestMethodNotSupported", exception);
    return new ResponseEntity<>(new Error(exception.getMessage(), "None"),
        HttpStatus.METHOD_NOT_ALLOWED);
  }

  /**
   * Handle all EntityNotFoundExceptions and return the error object contained in the exception.
   * 
   * @param exception EntityNotFoundException propagated to this class.
   * @return ResponseEntity which will contain an Error object.
   */
  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<Error> handleEntityNotFoundException(
      EntityNotFoundException exception) {
    logbackLogger.error("handleEntityNotFoundException", exception);
    return new ResponseEntity<>(exception.getError(), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(EntityExistsException.class)
  public ResponseEntity<Error> handleEntityExistsException(
      EntityExistsException exception) {
    logbackLogger.error("handleEntityExistsException", exception);
    return new ResponseEntity<>(new Error(exception.getMessage(), "None"),
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(DataAccessException.class)
  public ResponseEntity<Error> handleDataAccessException(
      DataAccessException exception) {
    logbackLogger.error("handleDataAccessException", exception);
    return new ResponseEntity<>(new Error(exception.getMessage(), "None"),
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Handle all ServiceException and return the error object contained in the exception.
   * 
   * @param exception ServiceException propagated to this class.
   * @return ResponseEntity which will contain an Error object.
   */
  @ExceptionHandler(ServiceException.class)
  public ResponseEntity<Error> handleServiceException(
      ServiceException exception) {
    logbackLogger.error("handleServiceException", exception);
    return new ResponseEntity<>(exception.getError(),
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(DockerException.class)
  public ResponseEntity<Error> handleDockerException(
      DockerException exception) {
    logbackLogger.error("handleDockerException", exception);
    return new ResponseEntity<>(new Error(exception.getMessage(), "None"),
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(InterruptedException.class)
  public ResponseEntity<Error> handleInterruptedException(
      InterruptedException exception) {
    logbackLogger.error("handleInterruptedException", exception);
    return new ResponseEntity<>(new Error(exception.getMessage(), "None"),
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Handle all InvalidValuesExceptions and return the error object contained in the exception.
   * 
   * @param exception InvalidValuesException propagated to this class.
   * @return ResponseEntity which will contain an Error object.
   */
  @ExceptionHandler(InvalidValuesException.class)
  public ResponseEntity<Error> handleInvalidValuesException(
      InvalidValuesException exception) {
    logbackLogger.error("handleInvalidValuesException", exception);
    return new ResponseEntity<>(exception.getError(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(NoSuchElementException.class)
  public ResponseEntity<Error> handleNoSuchElementException(
      NoSuchElementException exception) {
    logbackLogger.error("handleNoSuchElementException", exception);
    return new ResponseEntity<>(new Error(exception.getMessage(), "None"),
        HttpStatus.NOT_FOUND);
  }
}
