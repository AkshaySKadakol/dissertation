package com.dissertation.bits.server.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dissertation.bits.server.service.DockerService;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Info;
import com.spotify.docker.client.messages.swarm.Node;
import com.spotify.docker.client.messages.swarm.Service;
import com.spotify.docker.client.messages.swarm.Task;

@RestController
@RequestMapping(value = "status")
public class StatusController {

  private static final Logger logger =
      LoggerFactory.getLogger(StatusController.class);

  private DockerService dockerService;

  @Autowired
  public StatusController(DockerService dockerService) {
    this.dockerService = dockerService;
  }

  @RequestMapping(value = "docker", method = RequestMethod.GET)
  public Info dockerInfo() throws DockerException, InterruptedException {
    logger.info("Get Docker Info");
    return dockerService.getDockerInfo();
  }

  @RequestMapping(value = "nodes", method = RequestMethod.GET)
  public List<Node> listNodes() throws DockerException, InterruptedException {
    logger.info("Get List of Nodes");
    return dockerService.listNodes();
  }

  @RequestMapping(value = "tasks", method = RequestMethod.GET)
  public List<Task> listTasks() throws DockerException, InterruptedException {
    logger.info("Get List of Tasks");
    return dockerService.listTasks();
  }

  @RequestMapping(value = "services", method = RequestMethod.GET)
  public List<Service> listServices()
      throws DockerException, InterruptedException {
    logger.info("List Services");
    return dockerService.listServices();
  }


}
