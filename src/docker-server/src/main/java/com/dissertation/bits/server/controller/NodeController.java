package com.dissertation.bits.server.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dissertation.bits.server.model.ClientInfo;
import com.dissertation.bits.server.model.ServiceInfo;
import com.dissertation.bits.server.repository.ClientRepository;
import com.dissertation.bits.server.repository.EntityExistsException;
import com.dissertation.bits.server.repository.EntityNotFoundException;
import com.dissertation.bits.server.repository.ServiceRepository;
import com.dissertation.bits.server.service.NodeService;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.swarm.Task;

@RestController
@RequestMapping(value = "nodes")
public class NodeController {

  private static final Logger logger =
      LoggerFactory.getLogger(NodeController.class);

  private NodeService nodeService;
  private ClientRepository clientRepository;
  private ServiceRepository serviceRepository;

  @Autowired
  public NodeController(NodeService nodeService,
      ClientRepository clientRepository, ServiceRepository serviceRepository) {
    this.nodeService = nodeService;
    this.clientRepository = clientRepository;
    this.serviceRepository = serviceRepository;
  }

  @RequestMapping(value = "{nodeId}/services", method = RequestMethod.POST)
  public ServiceInfo createService(@PathVariable String nodeId,
      @RequestBody ServiceInfo serviceInfo)
      throws DockerException, InterruptedException {
    logger.info("Create Service nodeId = {}, serviceInfo = {}", nodeId,
        serviceInfo);
    ClientInfo clientInfo = getClientInfoByNodeIdFromRepository(nodeId);
    ServiceInfo existingServiceInfo =
        serviceRepository.getListOfServicesInfoByClientInfo(clientInfo).stream()
            .filter(
                s -> s.getServiceName().equals(serviceInfo.getServiceName()))
            .findFirst().orElse(null);
    if (existingServiceInfo != null) {
      throw new EntityExistsException(
          "A service with the given name already exists. Please use a different name.",
          "serviceName");
    }
    nodeService.validateServiceInfo(serviceInfo);
    serviceInfo.setClientInfo(clientInfo);
    nodeService.createService(serviceInfo);
    return serviceRepository.save(serviceInfo);
  }

  @RequestMapping(value = "{nodeId}/services", method = RequestMethod.GET)
  public List<ServiceInfo> listServices(@PathVariable String nodeId)
      throws DockerException, InterruptedException {
    logger.info("List Services nodeId = {}", nodeId);
    ClientInfo clientInfo = getClientInfoByNodeIdFromRepository(nodeId);
    return serviceRepository.getListOfServicesInfoByClientInfo(clientInfo);
  }

  @RequestMapping(value = "{nodeId}/services/{serviceName}",
      method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteService(@PathVariable String nodeId,
      @PathVariable String serviceName)
      throws DockerException, InterruptedException {
    logger.info("Delete Service nodeId = {}, serviceName = {}", nodeId,
        serviceName);
    ClientInfo clientInfo = getClientInfoByNodeIdFromRepository(nodeId);
    ServiceInfo existingServiceInfo =
        serviceRepository.getListOfServicesInfoByClientInfo(clientInfo).stream()
            .filter(s -> s.getServiceName().equals(serviceName)).findFirst()
            .orElse(null);
    if (existingServiceInfo == null) {
      throw new EntityNotFoundException(
          "A service with the given name does not exist.", "serviceName");
    }
    nodeService.deleteService(serviceName);;
    serviceRepository.delete(existingServiceInfo);
    return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
  }

  @RequestMapping(value = "{nodeId}/services/{serviceName}",
      method = RequestMethod.PUT)
  public ServiceInfo updateService(@PathVariable String nodeId,
      @RequestBody ServiceInfo serviceInfo, @PathVariable String serviceName)
      throws DockerException, InterruptedException {
    logger.info(
        "Update Service nodeId = {}, ServiceName = {}, ServiceInfo = {}",
        nodeId, serviceName, serviceInfo);
    ClientInfo clientInfo = getClientInfoByNodeIdFromRepository(nodeId);
    ServiceInfo existingServiceInfo =
        serviceRepository.getListOfServicesInfoByClientInfo(clientInfo).stream()
            .filter(s -> s.getServiceName().equals(serviceName)).findFirst()
            .orElse(null);
    if (existingServiceInfo == null) {
      throw new EntityNotFoundException(
          "A service with the given name does not exist.", "serviceName");
    }
    nodeService.updateService(existingServiceInfo, serviceInfo);
    return serviceRepository.save(existingServiceInfo);
  }

  @RequestMapping(value = "{nodeId}/services/{serviceName}/tasks",
      method = RequestMethod.GET)
  public List<Task> getTasksForService(@PathVariable String nodeId,
      @PathVariable String serviceName)
      throws DockerException, InterruptedException {
    logger.info("Get Tasks for Service nodeId = {}, serviceName = {}", nodeId,
        serviceName);
    getClientInfoByNodeIdFromRepository(nodeId);
    return nodeService.getTasksForService(serviceName);
  }

  private ClientInfo getClientInfoByNodeIdFromRepository(String nodeId) {
    ClientInfo clientInfo = clientRepository.getClientInfoByNodeId(nodeId);
    logger.info("Found Client Info {}", clientInfo);
    if (clientInfo == null) {
      throw new EntityNotFoundException("No client info found for the given id",
          "nodeId");
    }
    return clientInfo;
  }

}
