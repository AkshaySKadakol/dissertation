package com.dissertation.bits.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dissertation.bits.server.model.ClientInfo;
import com.dissertation.bits.server.repository.ClientRepository;
import com.dissertation.bits.server.repository.EntityNotFoundException;
import com.dissertation.bits.server.repository.ServiceRepository;
import com.dissertation.bits.server.service.ClientService;
import com.spotify.docker.client.exceptions.DockerException;

@RestController
@RequestMapping(value = "clients")
public class ClientController {

  private static final Logger logger =
      LoggerFactory.getLogger(ClientController.class);

  private ClientService clientService;
  private ClientRepository clientRepository;
  private ServiceRepository serviceRepository;

  @Autowired
  public ClientController(ClientService clientService,
      ClientRepository clientRepository, ServiceRepository serviceRepository) {
    this.clientService = clientService;
    this.clientRepository = clientRepository;
    this.serviceRepository = serviceRepository;
  }

  @RequestMapping(value = "", method = RequestMethod.POST)
  public ClientInfo saveClientInfo(@RequestBody ClientInfo clientInfo)
      throws DockerException, InterruptedException {
    logger.info("Save Client Info {}", clientInfo);
    clientService.validateClientInfo(clientInfo);
//    clientService.throwExceptionIfNodeExists(clientInfo);
    clientService.createPackingService(clientInfo);
    return clientRepository.save(clientInfo);
  }

  @RequestMapping(value = "{id}", method = RequestMethod.GET)
  public ClientInfo getClientInfo(@PathVariable long id) {
    logger.info("Get Client Info {}", id);
    ClientInfo clientInfo = getClientInfoFromRepository(id);
    return clientInfo;
  }

  @RequestMapping(value = "node/{nodeId}", method = RequestMethod.GET)
  public ClientInfo getClientInfoByNodeId(@PathVariable String nodeId) {
    logger.info("Get Client Info for Node ID {}", nodeId);
    return getClientInfoByNodeIdFromRepository(nodeId);
  }

  @RequestMapping(value = "node/{nodeId}", method = RequestMethod.PUT)
  public ClientInfo updateClientInfo(@PathVariable String nodeId,
      @RequestBody ClientInfo clientInfo)
      throws DockerException, InterruptedException {
    logger.info("Update Client Info for id = {}, {}", nodeId, clientInfo);
    ClientInfo existingClientInfo = getClientInfoByNodeIdFromRepository(nodeId);
    clientInfo.setId(existingClientInfo.getId());
    clientInfo.setNodeId(existingClientInfo.getNodeId());
    clientService.validateClientInfo(clientInfo);
    clientService.updatePackingService(clientInfo);
    return clientRepository.save(clientInfo);
  }

  @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteClientInfoById(@PathVariable long id)
      throws DockerException, InterruptedException {
    logger.info("Delete Client Info id = {}", id);
    ClientInfo clientInfo = getClientInfoFromRepository(id);
    deleteClientInfo(clientInfo);
    return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
  }

  @RequestMapping(value = "node/{nodeId}", method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteClientInfoByNodeId(
      @PathVariable String nodeId)
      throws DockerException, InterruptedException {
    logger.info("Delete Client Info for Node Id = {}", nodeId);
    ClientInfo clientInfo = getClientInfoByNodeIdFromRepository(nodeId);
    deleteClientInfo(clientInfo);
    return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
  }

  private void deleteClientInfo(ClientInfo clientInfo)
      throws DockerException, InterruptedException {
    clientService.deletePackingService(clientInfo);
    serviceRepository.getListOfServicesInfoByClientInfo(clientInfo).stream()
        .forEach(serviceInfo -> {
          clientService.deleteService(serviceInfo.getServiceName());
          serviceRepository.delete(serviceInfo);
        });
    clientRepository.delete(clientInfo.getId());
  }

  private ClientInfo getClientInfoFromRepository(long id) {
    ClientInfo clientInfo = clientRepository.findOne(id);
    logger.info("Found Client Info {}", clientInfo);
    if (clientInfo == null) {
      throw new EntityNotFoundException("No client info found for the given id",
          "id");
    }
    return clientInfo;
  }

  private ClientInfo getClientInfoByNodeIdFromRepository(String nodeId) {
    ClientInfo clientInfo = clientRepository.getClientInfoByNodeId(nodeId);
    logger.info("Found Client Info {}", clientInfo);
    if (clientInfo == null) {
      throw new EntityNotFoundException("No client info found for the given id",
          "id");
    }
    return clientInfo;
  }

}
