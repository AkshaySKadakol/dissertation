package com.dissertation.bits.server.service;

import com.dissertation.bits.server.model.Error;

/**
 * Exception class to be used by services that are in this package to communicate with the caller.
 * All other exceptions should be converted to ServiceException before communicating to caller.
 *
 */
public class ServiceException extends RuntimeException {

  private static final long serialVersionUID = 2807477578852442900L;
  private final Error error;

  /**
   * Instantiate an object of this class with the input message and field.
   * 
   * @param message Message containing the details of the exception.
   * @param field The field which caused this exception.
   */
  public ServiceException(final String message, final String field) {
    super(message);
    error = new Error(message, field);
  }

  /**
   * Instantiate an object of this class with the input throwable and field.
   * 
   * @param throwable The Throwable object associated with the exception.
   * @param field The field which caused this exception.
   */
  public ServiceException(final Throwable throwable, final String field) {
    super(throwable);
    error = new Error(throwable.getMessage(), field);
  }

  /**
   * Gets the error.
   *
   * @return the error
   */
  public Error getError() {
    return error;
  }

}
