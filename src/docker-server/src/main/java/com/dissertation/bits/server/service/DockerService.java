package com.dissertation.bits.server.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Info;
import com.spotify.docker.client.messages.swarm.Node;
import com.spotify.docker.client.messages.swarm.Service;
import com.spotify.docker.client.messages.swarm.Task;

@org.springframework.stereotype.Service
public class DockerService {

  @Value("${dockerUri}")
  private String dockerUri;

  private DockerClient dockerClient;

  protected DockerService() {}

  protected DockerClient getDockerClient() {
    return dockerClient;
  }

  @PostConstruct
  private void setup() throws DockerException, InterruptedException {
    dockerClient = DefaultDockerClient.builder().uri(dockerUri).build();
  }

  @PreDestroy
  private void shutdown() {
    dockerClient.close();
  }

  public Info getDockerInfo() throws DockerException, InterruptedException {
    return dockerClient.info();
  }

  public List<Node> listNodes() throws DockerException, InterruptedException {
    return dockerClient.listNodes();
  }

  public List<Task> listTasks() throws DockerException, InterruptedException {
    return dockerClient.listTasks();
  }

  public List<Service> listServices()
      throws DockerException, InterruptedException {
    return dockerClient.listServices();
  }
}
