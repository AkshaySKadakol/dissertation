package com.dissertation.bits.server.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.dissertation.bits.server.model.ClientInfo;
import com.dissertation.bits.server.model.ServiceInfo;

@Transactional
public interface ServiceRepository extends CrudRepository<ServiceInfo, Long> {

  public ServiceInfo getServiceInfoByServiceId(String serviceId);

  public List<ServiceInfo> getListOfServicesInfoByClientInfo(
      ClientInfo clientInfo);

}
