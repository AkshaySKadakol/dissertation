package com.dissertation.bits.server.repository;

import com.dissertation.bits.server.model.Error;

public class NullAccessException extends RuntimeException {

  private static final long serialVersionUID = -5287362999801702077L;
  /** The error. */
  private final Error error;

  /**
   * Instantiate an object of this class with the input message and field.
   * 
   * @param message Message containing the details of the exception.
   * @param field The field which caused this exception.
   */
  public NullAccessException(final String message, final String field) {
    super(message);
    error = new Error(message, field);
  }

  /**
   * Instantiate an object of this class with the input throwable and field.
   * 
   * @param throwable The Throwable object associated with the exception.
   * @param field The field which caused this exception.
   */
  public NullAccessException(final Throwable throwable, final String field) {
    super(throwable);
    error = new Error(throwable.getMessage(), field);
  }

  /**
   * Gets the error.
   *
   * @return the error
   */
  public Error getError() {
    return error;
  }
}
