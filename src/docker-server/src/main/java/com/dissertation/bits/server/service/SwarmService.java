package com.dissertation.bits.server.service;

import org.springframework.stereotype.Service;

import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.swarm.Swarm;
import com.spotify.docker.client.messages.swarm.SwarmInfo;

@Service
public class SwarmService extends ClientService {

  protected SwarmService() {}

  public Swarm inspectSwarm() throws DockerException, InterruptedException {
    return super.getDockerClient().inspectSwarm();
  }

  public String getWorkerToken() throws DockerException, InterruptedException {
    return inspectSwarm().joinTokens().worker();
  }

  public String getMangerToken() throws DockerException, InterruptedException {
    return inspectSwarm().joinTokens().manager();
  }

  public SwarmInfo getSwarmInfo() throws DockerException, InterruptedException {
    return super.getDockerClient().info().swarm();
  }

}
