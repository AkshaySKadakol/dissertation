package com.dissertation.bits.server.model;

import java.io.Serializable;

/**
 * Class containing the error code, message and field which caused the error.
 */
public class Error implements Serializable {

  private static final long serialVersionUID = 887441799584411270L;

  private String message = null;
  private String field = null;

  /**
   * Constructor. Create and Error object with the input parameters.
   * 
   * @param message Error message.
   * @param field Field which caused the error.
   */
  public Error(final String message, final String field) {
    this.message = message;
    this.field = field;
  }

  public String getMessage() {
    return message;
  }

  public String getField() {
    return field;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("class Error {\n");
    sb.append("  message: ").append(message).append('\n');
    sb.append("  field: ").append(field).append('\n');
    sb.append("}\n");
    return sb.toString();
  }

}
