package com.dissertation.bits.server.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "clients")
public class ClientInfo {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @NotNull
  private String nodeId;

  @NotNull
  private int allocateMemoryPercentage;

  @NotNull
  private int allocateCpuPercentage;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNodeId() {
    return nodeId;
  }

  public void setNodeId(String nodeId) {
    this.nodeId = nodeId;
  }

  public long getAllocateMemoryPercentage() {
    return allocateMemoryPercentage;
  }

  public void setAllocateMemoryPercentage(int allocateMemoryPercentage) {
    this.allocateMemoryPercentage = allocateMemoryPercentage;
  }

  public long getAllocateCpuPercentage() {
    return allocateCpuPercentage;
  }

  public void setAllocateCpuPercentage(int allocateCpuPercentage) {
    this.allocateCpuPercentage = allocateCpuPercentage;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("class ClientInfo {\n");
    sb.append("  id: ").append(id).append('\n');
    sb.append("  nodeId: ").append(nodeId).append('\n');
    sb.append("  allocateMemoryPercentage: ").append(allocateMemoryPercentage)
        .append('\n');
    sb.append("  allocateCpuPercentage: ").append(allocateCpuPercentage)
        .append('\n');
    sb.append("}\n");
    return sb.toString();
  }

}
