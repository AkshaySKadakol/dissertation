package com.dissertation.bits.server.service;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.dissertation.bits.server.model.ClientInfo;
import com.dissertation.bits.server.repository.EntityExistsException;
import com.dissertation.bits.server.repository.EntityNotFoundException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.swarm.ContainerSpec;
import com.spotify.docker.client.messages.swarm.Node;
import com.spotify.docker.client.messages.swarm.Placement;
import com.spotify.docker.client.messages.swarm.ResourceRequirements;
import com.spotify.docker.client.messages.swarm.Resources;
import com.spotify.docker.client.messages.swarm.Service;
import com.spotify.docker.client.messages.swarm.Service.Criteria;
import com.spotify.docker.client.messages.swarm.ServiceSpec;
import com.spotify.docker.client.messages.swarm.TaskSpec;

@org.springframework.stereotype.Service
public class ClientService extends DockerService {

  private static final Logger logger =
      LoggerFactory.getLogger(ClientService.class);

  @Value("${registry}")
  private String registry;

  protected ClientService() {}

  @PostConstruct
  private void setup() {
    logger.info("Registry = {}", registry);
  }

  public boolean validateClientInfo(ClientInfo clientInfo)
      throws DockerException, InterruptedException {
    if (clientInfo.getNodeId() == null) {
      throw new InvalidValuesException("Invalid Node ID", "nodeId");
    }
    if (clientInfo.getAllocateMemoryPercentage() < 25
        || clientInfo.getAllocateMemoryPercentage() > 50) {
      throw new InvalidValuesException(
          "Memory allocated must be between 25% and 50% of total node memory",
          "reserveMemory");
    }
    if (clientInfo.getAllocateCpuPercentage() < 25
        || clientInfo.getAllocateCpuPercentage() > 50) {
      throw new InvalidValuesException(
          "CPU allocated must be between 25% and 50%", "reserveCpu");
    }
    return true;
  }

  public void throwExceptionIfNodeExists(ClientInfo clientInfo)
      throws DockerException, InterruptedException {
    Node node = super.getDockerClient().listNodes().stream()
        .filter(n -> n.id().equals(clientInfo.getNodeId())).findFirst()
        .orElse(null);
    if (node != null) {
      throw new EntityExistsException(
          "A Node with the given ID already exists. Please use a different name.",
          "nodeId");
    }
  }

  private Node getNode(ClientInfo clientInfo)
      throws DockerException, InterruptedException {
    Node node = super.getDockerClient().listNodes().stream()
        .filter(n -> n.id().equals(clientInfo.getNodeId())).findFirst()
        .orElse(null);
    logger.info("Node found = {}", node);
    if (node == null)
      throw new EntityNotFoundException("No node found for the given Node ID",
          "nodeId");
    return node;
  }

  public void createPackingService(ClientInfo clientInfo)
      throws DockerException, InterruptedException {
    Node node = getNode(clientInfo);
    Placement placement = Placement.create(
        Arrays.asList(String.format("node.id == %s", clientInfo.getNodeId())));
    long memoryBytes = node.description().resources().memoryBytes()
        * (100 - clientInfo.getAllocateMemoryPercentage()) / 100;
    long nanoCpus = (100 - clientInfo.getAllocateCpuPercentage())
        * node.description().resources().nanoCpus() / 100;
    Resources resources =
        Resources.builder().memoryBytes(memoryBytes).nanoCpus(nanoCpus).build();
    ContainerSpec containerSpec =
        ContainerSpec.builder().image(registry + "/alpine:latest")
            .command("tail", "-f", "/dev/null").build();
    TaskSpec taskSpec = TaskSpec.builder()
        .containerSpec(containerSpec).resources(ResourceRequirements.builder()
            .limits(resources).reservations(resources).build())
        .placement(placement).build();
    ServiceSpec serviceSpec = ServiceSpec.builder()
        .name(String.format("packer_%s", clientInfo.getNodeId()))
        .taskTemplate(taskSpec).build();
    logger.info("Creating a service with the following specs - {}",
        serviceSpec);
    super.getDockerClient().createService(serviceSpec);
  }

  public void updatePackingService(ClientInfo clientInfo)
      throws DockerException, InterruptedException {
    Node node = getNode(clientInfo);
    long memoryBytes = (100 - clientInfo.getAllocateMemoryPercentage())
        * node.description().resources().memoryBytes() / 100;
    long nanoCpus = (100 - clientInfo.getAllocateCpuPercentage())
        * node.description().resources().nanoCpus() / 100;
    Resources resources =
        Resources.builder().memoryBytes(memoryBytes).nanoCpus(nanoCpus).build();
    Service service = super.getDockerClient().listServices(Criteria.builder()
        .serviceName(String.format("packer_%s", clientInfo.getNodeId()))
        .build()).stream().findFirst().orElse(null);
    if (service == null) {
      throw new ServiceException("No Service found for the given service name",
          "nodeId");
    }
    TaskSpec taskSpec = service.spec().taskTemplate();
    logger.info("Existing resource allocation {}", taskSpec.resources());
    TaskSpec newTaskSpec =
        TaskSpec.builder().containerSpec(taskSpec.containerSpec())
            .resources(ResourceRequirements.builder().limits(resources)
                .reservations(resources).build())
            .placement(taskSpec.placement()).build();
    super.getDockerClient().updateService(service.id(),
        service.version().index(),
        ServiceSpec.builder().name(service.spec().name())//
            .taskTemplate(newTaskSpec)//
            .endpointSpec(service.spec().endpointSpec())//
            .updateConfig(service.spec().updateConfig())//
            .build());
  }

  public void deletePackingService(ClientInfo clientInfo)
      throws DockerException, InterruptedException {
    super.getDockerClient()
        .removeService(String.format("packer_%s", clientInfo.getNodeId()));
  }

  public void deleteService(String serviceName) {
    try {
      super.getDockerClient().removeService(serviceName);
    } catch (Exception e) {
      logger.error("Exception while deleting services", e);
    }
  }
}
