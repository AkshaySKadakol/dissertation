package com.dissertation.bits.server.repository;

import com.dissertation.bits.server.model.Error;

/**
 * Exception class to be used by Storage that are in this package to communicate with the caller.
 */
public class EntityNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 289637433081202199L;
  private final Error error;

  /**
   * Instantiate an object of this class with the input message and field.
   * 
   * @param message Message containing the details of the exception.
   * @param field The field which caused this exception.
   */
  public EntityNotFoundException(final String message, final String field) {
    super(message);
    error = new Error(message, field);
  }

  /**
   * Instantiate an object of this class with the input throwable and field.
   * 
   * @param throwable The Throwable object associated with the exception.
   * @param field The field which caused this exception.
   */
  public EntityNotFoundException(final Throwable throwable,
      final String field) {
    super(throwable);
    error = new Error(throwable.getMessage(), field);
  }

  public Error getError() {
    return error;
  }

}
