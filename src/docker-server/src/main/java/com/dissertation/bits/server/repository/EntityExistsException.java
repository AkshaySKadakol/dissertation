package com.dissertation.bits.server.repository;

import com.dissertation.bits.server.model.Error;

/**
 * Exception class to be used by Storage that are in this package to communicate with the caller.
 */
public class EntityExistsException extends RuntimeException {

  private static final long serialVersionUID = 5185425491219536110L;
  private final Error error;

  /**
   * Instantiate an object of this class with the input message and field.
   * 
   * @param message Message containing the details of the exception.
   * @param field The field which caused this exception.
   */
  public EntityExistsException(final String message, final String field) {
    super(message);
    error = new Error(message, field);
  }

  public Error getError() {
    return error;
  }

}
