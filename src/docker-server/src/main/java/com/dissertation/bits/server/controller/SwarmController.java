package com.dissertation.bits.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dissertation.bits.server.service.SwarmService;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.swarm.Swarm;
import com.spotify.docker.client.messages.swarm.SwarmInfo;

@RestController
@RequestMapping(value = "swarm")
public class SwarmController {

  private static final Logger logger =
      LoggerFactory.getLogger(SwarmController.class);

  private SwarmService swarmService;

  @Autowired
  public SwarmController(SwarmService swarmService) {
    this.swarmService = swarmService;
  }

  @RequestMapping(value = "", method = RequestMethod.GET)
  public Swarm inspectswarm() throws DockerException, InterruptedException {
    logger.info("Inspect Swarm");
    return swarmService.inspectSwarm();
  }

  @RequestMapping(value = "info", method = RequestMethod.GET)
  public SwarmInfo getSwarmInfo() throws DockerException, InterruptedException {
    logger.info("Inspect Swarm");
    return swarmService.getSwarmInfo();
  }

  @RequestMapping(value = "worker-token", method = RequestMethod.GET)
  public String getWorkerToken() throws DockerException, InterruptedException {
    logger.info("Get Worker Token");
    return swarmService.getWorkerToken();
  }

  @RequestMapping(value = "manager-token", method = RequestMethod.GET)
  public String getManagerToken() throws DockerException, InterruptedException {
    logger.info("Get Manager Token");
    return swarmService.getMangerToken();
  }

}
