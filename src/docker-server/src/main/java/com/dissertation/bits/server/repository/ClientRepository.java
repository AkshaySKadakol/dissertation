package com.dissertation.bits.server.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.dissertation.bits.server.model.ClientInfo;

@Transactional
public interface ClientRepository extends CrudRepository<ClientInfo, Long> {

  public ClientInfo getClientInfoByNodeId(String nodeId);

}
