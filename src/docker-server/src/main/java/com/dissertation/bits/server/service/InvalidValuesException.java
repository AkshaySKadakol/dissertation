package com.dissertation.bits.server.service;

import com.dissertation.bits.server.model.Error;

public class InvalidValuesException extends RuntimeException {

  private static final long serialVersionUID = 8704137013241671811L;
  private final Error error;

  public InvalidValuesException(Throwable exception, String message,
      String field) {
    super(exception);
    error = new Error(message, field);
  }

  public InvalidValuesException(String message, String field) {
    super(message);
    error = new Error(message, field);
  }

  public Error getError() {
    return error;
  }

}
